# -*- coding: utf-8 -*-
require 'rubygems'
require 'hikidoc'

module RedmineHikidocFormatter
  class HTMLOutput < HikiDoc::HTMLOutput
    def initialize(suffix)
      super suffix
    end
    def hyperlink(uri, title)
      if /^(https?|ftp):\/\// =~ uri
        %Q(<a href="#{escape_html_param(uri)}">#{title}</a>)
      else
        %Q([[#{uri}]])
      end
    end
  end

  class WikiFormatter
    def initialize(text)
      @text = text
    end

    def to_html(&block)
      @macros_runner = block
      parsedText = @text
      opt = {:use_wiki_name => false, :level => 2}
      parsedText = HikiDoc.new(HTMLOutput.new(">"), opt).compile(parsedText)
      # Add the `external` class to every link
      parsedText = parsedText.gsub(/<a (href="(https?|ftp):\/\/)/, "<a class=\"external\" \\1")
      # WikiMacro
      parsedText = inline_macros(parsedText)
    rescue => e
      return("<pre>problem parsing wiki text: #{e.message}\n"+
             "original text: \n"+
             @text+
             "</pre>")
    end

    MACROS_RE = /
          (
          <span class="plugin">
          \{\{                        # opening tag
          ([\w]+)                     # macro name
          (\(([^\}]*)\))?             # optional arguments
          \}\}                        # closing tag
          )
          <\/span>
        /x

    def inline_macros(text)
      text.gsub!(MACROS_RE) do
        all, macro = $1, $2, $3.downcase
        args = ($5 || '').split(',').each(&:strip)
        begin
          @macros_runner.call(macro, args)
        rescue => e
          "<div class=\"flash error\">Error executing the <strong>#{macro}</strong> macro (#{e})</div>"
        end || all
      end
      text
    end

  end
end
