# Redmine HikiDoc formatter
require 'redmine'

RAILS_DEFAULT_LOGGER.info 'Starting HikiDoc formatter for RedMine'

Redmine::Plugin.register :redmine_hikidoc_formatter do
  name 'HikiDoc formatter'
  author 'MACHIDA \'matchy\' Hideki'
  description 'This provides HikiDoc as a wiki format'
  version '0.0.2'

  wiki_format_provider 'HikiDoc', RedmineHikidocFormatter::WikiFormatter, RedmineHikidocFormatter::Helper

  Redmine::WikiFormatting::Macros.register do
    desc <<'EOF'
inline <BR> macro for HikiDoc.

  !{{br}}
EOF
    macro :br do |wiki_content_obj, args|
      '<br />'
    end
  end

end
