Redmine HikiDoc formatter
================================

This is a redmine plugin for supporting HikiDoc as a wiki format.


What is redmine?
----------------

Redmine is a flexible project management web application.
See [the official site](http://www.redmine.org/) for more details.


What is HikiDoc?
-----------------------

'HikiDoc' is a text-to-HTML conversion tool for web writers. HikiDoc
allows you to write using an easy-to-read, easy-to-write plain text
format, then convert it to structurally valid HTML (or XHTML).


Prerequisites
-------------

*  Redmine >= 1.0.0
*  hikidoc gem - see http://projects.netlab.jp/hikidoc/


Installation
------------

1.  Copy the plugin directory into the vendor/plugins directory
2.  Start Redmine
3.  Installed plugins are listed on 'Admin -> Information' screen.


Credits
-------

*  MACHIDA 'matchy' Hideki (http://github.com/matchy2)



Suggestions, Bugs, Refactoring?
-------------------------------

Fork away and create a Github Issue. Pull requests are welcome.
http://github.com/matchy2/redmine_hikidoc_formatter/tree/master

